package org.crown.common.cons;

/**
 * 
 * @author wongs
 */
public final class Constants {

	public final class Common {
		/**
		 * 处理成功代码，默认0
		 */
		public final static int SUCCESS_CODE = 000;
		/**
		 * 处理失败代码，默认-9
		 * <p>一般标记为异常错误代码</p>
		 */
		public final static int ERROR_CODE = -999;
		
	}
	
    public final class Env {

        private Env() {
        }

        public static final String BASE_HOME = "conf/";
    }
	
    /*
     * 支付模块
     */
    public final class Pay {
    	   
        /** 西部ca */
        public static final String CWCAID = "cwcaId";
        
        /** 生成订单-未付款-状态 */
        public static final String PAY_STATUS_NOT_PAYMENT = "0";
        
        /** 生成订单-未付款 */
        public static final String PAY_STATUS_NOT_PAYMENT_STRING = "生成订单-未付款";
        
        /** 付款-前台-状态-1 */
        public static final String PAY_STATUS_OK_PAYMENT = "1";
        
        /** 付款-前台 */
        public static final String PAY_STATUS_OK_PAYMENT_STRING = "付款-前台";
        
        /** 付款成功-后台-状态-2 */
        public static final String PAY_STATUS_BACK_OK_PAYMENT = "2";
        
        /** 付款成功-后台 */
        public static final String PAY_STATUS_BACK_OK_PAYMENT_STRING = "付款成功-后台";
        
        /** 付款失败-状态-3 */
        public static final String PAY_STATUS_DEFEAT = "3";
        
        /** 付款失败 */
        public static final String PAY_STATUS_DEFEAT_STRING = "付款失败";
        
        /** 支付前台通知渠道 */
        public static final String PAY_NOTIFY_FRONT = "1";
        
        /** 支付后台通知渠道 */
        public static final String PAY_NOTIFY_BACK = "2";
        
    }
    
    public final class NuoPay {
		/** nuopay前台响应信息 */
    	public static final String NUOPAY_FRONT_RESPCODE_STRING = "nuopay-front";
		/** nuopay后台响应信息 */
    	public static final String NUOPAY_BACK_RESPCODE_STRING = "nuopay-back";
		/** nuopay退款响应信息 */
    	public static final String NUOPAY_REFUND_RESPCODE_STRING = "nuopay-refund";
	} 
    
}

