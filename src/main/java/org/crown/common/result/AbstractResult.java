package org.crown.common.result;
import java.io.Serializable;
/**
 * 返回结果集定义
 * @author wongs
 * @param <T>
 */
public class AbstractResult<T> implements Result<T>, Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 结果类型
	 */
	protected T data;
	/**
	 * 错误代码
	 */
	protected int retCode;
	/**
	 * 错误信息
	 */
	protected String errMsg;

	public AbstractResult(int retCode, String errMsg, T data) {
		this.retCode = retCode;
		this.errMsg = errMsg;
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public int getRetCode() {
		return retCode;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("{RET_CODE:").append(this.retCode).append(",ERR_MSG:").append(this.errMsg).append("}");
		return sb.toString();
	}
}
