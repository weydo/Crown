package org.crown.common.result;

import org.crown.common.cons.Constants;

/**
 * 处理异常的操作返回定义
 * <p>
 * 业务处理一旦遇到异常，则只返回错误码及错误信息。
 * </p>
 * @param <T>
 * @author wongs
 * @see AbstractResult
 */
public class ExceptionResult<T> extends AbstractResult<T> {

	private static final long serialVersionUID = 6003764169295971021L;

	public ExceptionResult(String errMsg) {
		super(Constants.Common.ERROR_CODE,errMsg,null);
	}

}