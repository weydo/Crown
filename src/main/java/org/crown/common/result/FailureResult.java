package org.crown.common.result;

import org.crown.common.cons.Constants;

/**
 * 处理失败的操作返回定义
 * <p>
 * 业务处理失败，比如，密码错误等
 * </p>
 * @param <T>
 * @author wongs
 * @see AbstractResult
 */

public class FailureResult<T> extends AbstractResult<T> {
	
	private static final long serialVersionUID = 7555493831007435402L;
	
	public FailureResult(String errMsg) {
		super(Constants.Common.ERROR_CODE,errMsg,null);
	}

	public FailureResult(int retCode, String errMsg) {
		super(retCode,errMsg,null);
	}
	
	public FailureResult(int retCode, String errMsg,T data) {
		super(retCode, errMsg, data);
	}

}
