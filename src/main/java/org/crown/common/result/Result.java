package org.crown.common.result;

import java.io.Serializable;
/**
 * 返回结果统一数据格式
 * @author wongs
 * @param <T>
 */
public interface Result<T> extends Serializable {
	/**
	 * 处理的结果，比如查询返回的List结果集，支持泛型。
	 * @return
	 */
	T getData();
	/**
	 * 处理响应码，可以理解为错误代码，可默认定义 0000 成功，其他错误。
	 * @return
	 */
	int getRetCode();
	/**
	 * 处理响应信息，处理完结果，返回到消息内容。比如保存失败，可提示具体什么原因
	 * @return
	 */
	String getErrMsg();
	/**
	 * toString
	 * @return
	 */
	String toString();
	
}
