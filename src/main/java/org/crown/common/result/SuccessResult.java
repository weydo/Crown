package org.crown.common.result;

import org.crown.common.cons.Constants;
/**
 * 处理成功的操作返回定义
 * <p>
 * 业务处理成功，则错误码一定是 0。
 * </p>
 * @param <T>
 * @author wongs
 * @see AbstractResult
 */
public class SuccessResult<T> extends AbstractResult<T> {

	private static final long serialVersionUID = 3574784156036297125L;

	public SuccessResult(T data) {
		super(Constants.Common.SUCCESS_CODE, "操作成功.", data);
	}

	public SuccessResult(String msg, T data) {
		super(Constants.Common.SUCCESS_CODE, msg, data);
	}

}
