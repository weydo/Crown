package org.crown.common.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.*;

/**
 * 项目名称：打印快递单后台服务
 * 开发人员：dw
 * 创建时间：2019-5-17
 **/

public class Base64ImageTools
{
  public static String getImageStr(String imgFilePath)
  {
    byte[] data = null;

    try
    {
      InputStream in = new FileInputStream(imgFilePath);
      data = new byte[in.available()];
      in.read(data);
      in.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }


    return new Base64().encodeAsString(data);
  }

  public static boolean generateImage(String imgStr, String imgFilePath) {
    if (imgStr == null) {
      return false;
    }
    try {
      byte[] bytes = new Base64().decode(imgStr);

      for (int i = 0; i < bytes.length; i++) {
        if (bytes[i] < 0) {
          int tmp31_30 = i; byte[] tmp31_29 = bytes;tmp31_29[tmp31_30] = ((byte)(tmp31_29[tmp31_30] + 256));
        }
      }

      OutputStream out = new FileOutputStream(imgFilePath);
      out.write(bytes);
      out.flush();
      out.close();
      return true;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}