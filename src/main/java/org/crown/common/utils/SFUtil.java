package org.crown.common.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import org.crown.model.entity.CodeExpress;
import org.crown.model.entity.OrderSf;
import org.jboss.logging.Logger;


/**
 * 顺丰快递辅助类
 * @author wongs
 * @date 2018.7.25
 */
public class SFUtil {

    private static final Logger log = Logger.getLogger(SFUtil.class);

    /**
     * 下单报文组装
     * @param
     */
    public static String xmlHelper(Map<String, String> params,CodeExpress codeExpress){
        String xml = "<Request service=\"OrderService\" lang=\"zh-CN\">"
                +"<Head>"+codeExpress.getClientcode()+","+codeExpress.getCheckword()+"</Head>"
                +"<Body><Order orderid='" + params.get("orderid") +"' is_gen_bill_no='1' parcel_quantity='1'"
                +" j_province='"+codeExpress.getJProvince()+"'"
                +" j_city='"+codeExpress.getJCity()+"'"
                +" j_company='"+codeExpress.getJCompany()+"'"
                +" j_contact='"+codeExpress.getJContact()+"'"
                +" j_tel='"+codeExpress.getJTel()+"'"
                +" j_address='"+codeExpress.getJAddress()+"'"
                +" j_shippercode ='"+codeExpress.getJShippercode()+"'"
                +" d_province='"+params.get("d_province")+"'"
                +" d_city='"+params.get("d_city")+"'"
                +" d_company='"+params.get("d_company")+"'"
                +" d_contact='"+params.get("d_contact")+"'"
                +" d_tel='"+params.get("d_tel")+"'"
                +" d_address='"+params.get("d_address")+"'"
                +" express_type='"+params.get("express_type")+"'"
                +" pay_method='"+params.get("pay_method")+"'"
                +" custid ='"+codeExpress.getCustid()+"'"
                +" cargo='"+codeExpress.getCargo()+"'"
                +" remark='"+params.get("remark")+"' >"
                +"</Order></Body></Request>";

        log.info("=======>顺丰订单下单报文："+xml);
        return xml;
    }

    /**
     * 下单报文组装
     * @param
     */
    public static String xmlRouteHelper(OrderSf sf,CodeExpress codeExpress){
        String xml = "<Request service=\"RouteService\" lang=\"zh-CN\">"
                +"<Head>"+codeExpress.getClientcode()+","+codeExpress.getCheckword()+"</Head>"
                +"<Body><RouteRequest tracking_type='1' method_type='1'"
                +" tracking_number='"+sf.getMailno()+"'/>" 	 // 444001911740
                +"</Body></Request>";

        log.info("=======>顺丰订单路由查询报文："+xml);
        return xml;
    }


    public static Map<String,String> xmlParse(String xml){
        Map<String,String> map = new HashMap<String,String>();
        Document doc;
        try {
            doc = DocumentHelper.parseText(xml);
            Element root = doc.getRootElement();
            @SuppressWarnings("unchecked")
            List<Element> elementList = root.elements();

            String head = elementList.get(0).getText();
            map.put("order_result", head);
            String body = elementList.get(1).getName();
            if("Body".equals(body)){
                Element orderResponse = elementList.get(1).element("OrderResponse");
                List<Attribute> bookAttrs = orderResponse.attributes();
                for (Attribute attr : bookAttrs) {
                    map.put(attr.getName(), attr.getValue());
                }
            }else{
                map.put("order_message", elementList.get(1).getText());
                List<Attribute> bookAttrs = elementList.get(1).attributes();
                for (Attribute attr : bookAttrs) {
                    map.put("error_code", attr.getValue());
                }
            }

        } catch (DocumentException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return map;
    }

    public static String xmlRouteResult(String xml){
        Document doc;
        try {
            doc = DocumentHelper.parseText(xml);
            Element root = doc.getRootElement();
            List<Element> elementList = root.elements();

            String head = elementList.get(0).getText();
            return head;
        } catch (DocumentException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return null;
    }

    public static Map<String,Map<String,String>> xmlRouteParse(String xml){
        Map<String,Map<String,String>> map = new HashMap<String,Map<String,String>>();
        Document doc;
        try {
            doc = DocumentHelper.parseText(xml);
            Element root = doc.getRootElement();
            List<Element> elementList = root.elements();

            String head = elementList.get(0).getText();
            String body = elementList.get(1).getName();
            if("OK".equals(head)){
                Element orderResponse = elementList.get(1).element("RouteResponse");
                List<Element> orderResponseList = orderResponse.elements();
                for(int i=0;i<orderResponseList.size();i++){
                    Map<String,String> mmap = new HashMap<String,String>();
                    String route = "";
                    Element orderRoute = orderResponseList.get(i);
                    List<Attribute> attrs = orderRoute.attributes();
                    for (Attribute attr : attrs) {
                        mmap.put(attr.getName(), attr.getValue());
                    }
                    map.put(orderRoute.getName()+i, mmap);
                }
            }else{
                Map<String,String> mmap = new HashMap<String,String>();
                mmap.put("order_message", elementList.get(1).getText());
                List<Attribute> bookAttrs = elementList.get(1).attributes();
                for (Attribute attr : bookAttrs) {
                    mmap.put("error_code", attr.getValue());
                }
                map.put("ERROR", mmap);
            }

        } catch (DocumentException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return map;
    }

    public static void main(String[] args) {
        String res = "<?xml version='1.0' encoding='UTF-8'?><Response service=\"RouteService\"><Head>OK</Head><Body><RouteResponse mailno=\"444001911740\" orderid=\"180727092717341\"><Route remark=\"顺丰速运 已收取快件（测试数据）\" accept_time=\"2018-05-01 08:01:44\" accept_address=\"广东省深圳市软件产业基地\" opcode=\"50\"/><Route remark=\"已签收,感谢使用顺丰,期待再次为您服务（测试数据）\" accept_time=\"2018-05-02 12:01:44\" accept_address=\"广东省深圳市软件产业基地\" opcode=\"80\"/></RouteResponse></Body></Response>";

    }
}

