package org.crown.controller;


import com.fasterxml.jackson.databind.util.JSONPObject;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.mapping.ResultMap;
import org.crown.common.annotations.Resources;
import org.crown.common.result.Result;
import org.crown.enums.AuthTypeEnum;
import org.crown.framework.controller.SuperController;
import org.crown.framework.responses.ApiResponses;
import org.crown.model.entity.Menu;
import org.crown.model.entity.OrderLocal;
import org.crown.model.parm.MenuPARM;
import org.crown.service.IExpressService;
import org.crown.service.IOrderLocalService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.List;

/**
 * SF接口实现
 * @author dw
 * @date 2019.05.21
 */
@RestController
//@RequestMapping("/SF")
@RequestMapping(value = "/SF", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class ExpressRestController extends SuperController {

    private static final Logger log = Logger.getLogger(ExpressRestController.class);

    @Autowired
    private IExpressService expressService;

    @Autowired
    private IOrderLocalService orderLocalService;

/*    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation(value = "打印快递面单")
    @RequestMapping(value="/printOrder")
    public ApiResponses<Void> printOrder(@RequestParam Map<String, String> input) {

        expressService.printOrderService(input);
        return success(HttpStatus.CREATED);
    }*/

    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation("下订单")
    @PostMapping(value="/order")
    public JSONObject order(@RequestBody Map<String, String> input){
        log.info("本地保存订单信息开始=====>");
        OrderLocal orderLocal = new OrderLocal();
        orderLocal.setId(input.get("orderid"));
        orderLocal.setUserId(input.get("userid"));
        orderLocal.setOrderId(input.get("orderid"));
        orderLocal.setDProvince(input.get("d_province"));
        orderLocal.setDCity(input.get("d_city"));
        orderLocal.setDCompany(input.get("d_company"));
        orderLocal.setDContact(input.get("d_contact"));
        orderLocal.setDTel(input.get("d_tel"));
        orderLocal.setDAddress(input.get("d_address"));
        orderLocal.setExpressType(input.get("express_type"));
        orderLocal.setPayMethod(input.get("pay_method"));
        orderLocal.setRemark(input.get("remark"));

        OrderLocal orderLocal1 = orderLocalService.getById(input.get("orderid"));//根据申请id查询本地订单信息
        if (orderLocal1 == null) {
            orderLocalService.save(orderLocal);//本地保存向顺丰下单的信息
        }else {
            orderLocalService.updateById(orderLocal);//本地update信息
        }
        log.info("本地保存订单信息结束=====>");
        log.info("顺丰快递开始生产订单=====>");
        Result<Map<String, String>> result = expressService.orderService(input);
        log.info("顺丰快递订单生成结果=====>"+JSON.toJSONString(result));
        return (JSONObject) JSON.toJSON(result);
    }

    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation("路由")
    @PostMapping(value="/routeOrder")
    public JSONObject routeOrder(@RequestBody Map<String, String> input, HttpServletRequest request) {
        log.info("顺丰快递开始查询快递路由信息......");
        Result<Map<String,Map<String,String>>> result = expressService.routeOrderService(input);
        log.info("顺丰快递结束查询快递路由信息......");
        return (JSONObject) JSON.toJSON(result);
    }

    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation("打印")
    @PostMapping(value="/printOrder")
    public @ResponseBody JSONObject printOrder(@RequestBody Map<String, String> input, HttpServletRequest request){
        log.info("顺丰快递开始打印快递面单......");
        Result<Map<String, String>> result = expressService.printOrderService(input);
        log.info("顺丰快递结束打印快递面单......");
        return (JSONObject) JSON.toJSON(result);
    }


    //查询顺丰快递单号
    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation("查询快递单号")
    @PostMapping(value="/findSFOrderId")
    public @ResponseBody JSONObject findSFOrderId(@RequestBody JSONObject applyIds){
        log.info("查询快递单号开始......");
        List<String> applyIds1 = applyIds.getObject("applyIds", List.class);
        Result<Map<String, String>> result = expressService.findSFOrderId(applyIds1);
        log.info("查询快递单号结束......");
        return (JSONObject) JSON.toJSON(result);
    }
/*    @PostMapping("/findSFOrderId")
    public ResultVo checkPayStates(@RequestBody JSONObject applyIds){
        List<String> applyIds1 = applyIds.getObject("applyIds", List.class);
        return payOrderService.checkPayState(applyIds1);

    }*/
/*    @Resources(auth = AuthTypeEnum.OPEN)
    @ApiOperation("打印快递面单")
*//*    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "订单ID", required = true, paramType = "path")
    })*//*
    @PutMapping("/printOrder")
    public ApiResponses<Void> printOrder(@RequestBody Map<String, String> input) {
        log.info("顺丰快递开始打印快递面单......");
        expressService.printOrderService(input);
        log.info("顺丰快递结束打印快递面单......");
        return success();
    }*/

}
