package org.crown.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.crown.framework.mapper.BaseMapper;
import org.crown.model.entity.CodeExpress;

/**
 * <p>
 * 寄件方信息表Mapper 接口
 * </p>
 *
 * @author Caratacus
 */
@Mapper
public interface CodeExpressMapper extends BaseMapper<CodeExpress> {
}
