package org.crown.mapper;

import org.crown.model.entity.OrderLocal;
import org.apache.ibatis.annotations.Mapper;
import org.crown.framework.mapper.BaseMapper;

@Mapper
public interface OrderLocalMapper extends BaseMapper<OrderLocal> {

}