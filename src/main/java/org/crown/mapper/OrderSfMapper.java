package org.crown.mapper;

import org.apache.ibatis.annotations.Param;
import org.crown.model.entity.OrderSf;
import org.apache.ibatis.annotations.Mapper;
import org.crown.framework.mapper.BaseMapper;

import java.io.Serializable;

@Mapper
public interface OrderSfMapper extends BaseMapper<OrderSf> {

}
