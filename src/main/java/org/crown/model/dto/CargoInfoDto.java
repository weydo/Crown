package org.crown.model.dto;

import lombok.Data;
import org.crown.framework.model.convert.Convert;

import java.math.BigDecimal;

@Data
public class CargoInfoDto extends Convert {

    private String cargo;
    private Integer parcelQuantity;
    private Integer cargoCount;
    private String cargoUnit;
    private BigDecimal cargoWeight;
    private BigDecimal cargoAmount;
    private BigDecimal cargoTotalWeight;
    private String remark;
    private String sku;
}
