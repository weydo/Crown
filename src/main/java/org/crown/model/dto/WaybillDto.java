package org.crown.model.dto;

import lombok.Data;
import org.crown.framework.model.convert.Convert;

import java.util.List;

@Data
public class WaybillDto extends Convert {

    private String mailNo;
    private int expressType;
    private int payMethod;
    private String returnTrackingNo;
    private String monthAccount;
    private String orderNo;
    private String zipCode;
    private String destCode;
    private String payArea;
    private String deliverCompany;
    private String deliverName;
    private String deliverMobile;
    private String deliverTel;
    private String deliverProvince;
    private String deliverCity;
    private String deliverCounty;
    private String deliverAddress;
    private String deliverShipperCode;
    private String consignerCompany;
    private String consignerName;
    private String consignerMobile;
    private String consignerTel;
    private String consignerProvince;
    private String consignerCity;
    private String consignerCounty;
    private String consignerAddress;
    private String consignerShipperCode;
    private String logo;
    private String sftelLogo;
    private String topLogo;
    private String topsftelLogo;
    private String totalFee;
    private String appId;
    private String appKey;
    private String electric;
    private List<CargoInfoDto> cargoInfoDtoList;
    private String insureValue;
    private String insureFee;
    private String codValue;
    private String codMonthAccount;
    private String abFlag;
    private String xbFlag;
    private String proCode;
    private String destRouteLabel;
    private String destTeamCode;
    private String codingMapping;
    private String codingMappingOut;
    private String sourceTransferCode;
    private String printIcon;
    private String QRCode;
    private boolean encryptMobile = false;

    private boolean encryptCustName = false;
}
