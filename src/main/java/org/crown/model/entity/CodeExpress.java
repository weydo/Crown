package org.crown.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

import java.math.BigDecimal;

/**
 * <p>
 * 寄件方信息表
 * </p>
 *
 * @author dw
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("code_express")
public class CodeExpress extends Convert {

    private BigDecimal id;

    private String clientcode;

    private String checkword;

    private BigDecimal custid;

    private String payMethod;

    private String expressType;

    private String cargo;

    private String jProvince;

    private String jCity;

    private String jCompany;

    private String jContact;

    private String jTel;

    private String jAddress;

    private Long jShippercode;
}
