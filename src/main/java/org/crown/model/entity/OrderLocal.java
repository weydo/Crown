package org.crown.model.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

import java.time.LocalDateTime;

/**
 * <p>
 * order_local本地订单信息表
 * </p>
 *
 * @author dw
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("order_local")
public class OrderLocal extends Convert {


    private static final long serialVersionUID = 1L;


    private String id;

    //用户编号
    private String userId;

    //订单号
    private String orderId;
    //快递单号
    private String mailno;
    //到件方所在省份
    private String dProvince;
    //到件方所在城市名称
    private String dCity;
    //到件方公司名称
    private String dCompany;
    //到件方联系人
    private String dContact;
    //到件方联系电话
    private String dTel;
    // 到件方详细地址，如果不传输d_province/d_city字段，此详细地址需包含省市信息，
    private String dAddress;
    //快件产品编码
    private String expressType;
    //付款方式：1-寄付 2-到付 3-第三方支付
    private String payMethod;

    //订单发货状态
    private String orderState;

    //备注
    private String remark;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
}
