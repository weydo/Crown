package org.crown.model.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

/**
 * <p>
 * order_sf表
 * </p>
 *
 * @author dw
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("order_sf")
public class OrderSf extends Convert {

    private String id;

    private String orderId;

    private String orderResult;

    private String mailno;

    private String filterResult;

    private String origincode;

    private String destcode;

    private String errorCode;

    private String errorMessage;

    private String remark;

    private String syncDate;
}
