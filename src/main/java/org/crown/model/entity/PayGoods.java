package org.crown.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

import java.util.Date;

/**
 * <p>
 * 订单列表
 * </p>
 *
 * @author dw
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("pay_goods")
public class PayGoods extends Convert {
    private Integer id;

    private Integer userId;

    private Integer codeId;

    private String orderNumber;

    private String fpqqlsh;

    private String fwType;

    private String kddh;

    private String name;

    private String nsrsbh;

    private String telephone;

    private String phone;

    private String address;

    private String mode;

    private String qzAddress;

    private String payState;

    private String payMoney;

    private String goodsZkeyNumber;

    private String goodsFkeyNumber;

    private String goodsYears;

    private String goodsName;

    private String goodsPhone;

    private String goodsProvince;

    private String goodsCity;

    private String goodsAddress;

    private String goodsNote;

    private String goodsState;

    private Date goodsCreateTime;

    private String downloadExcState;

    private String userType;

    private String system;

    private String city;

    private String province;

    private String themeO;

    private String themeOu;

    private String fpNsrsbh;

    private String fpAddress;

    private String fpTelephone;

    private String fpBank;

    private String fpBankCode;
}
