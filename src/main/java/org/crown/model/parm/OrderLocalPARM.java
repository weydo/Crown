package org.crown.model.parm;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * order_local表PARM
 * </p>
 *
 * @author Caratacus
 */
@ApiModel
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OrderLocalPARM extends Convert {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "顺丰运单号")
    @NotBlank(groups = { OrderLocalPARM.Update.class}, message = "顺丰运单号不能为空")
    private String mailno;

    @ApiModelProperty(notes = "订单发货状态")
    @NotBlank(groups = { OrderLocalPARM.Update.class}, message = "订单发货状态不能为空")
    private String orderState;

    public interface Update {

    }
}
