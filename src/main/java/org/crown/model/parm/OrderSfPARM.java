package org.crown.model.parm;


import javax.validation.constraints.NotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.crown.framework.model.convert.Convert;

/**
 * <p>
 * order_sf表PARM
 * </p>
 *
 * @author Caratacus
 */
@ApiModel
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class OrderSfPARM extends Convert {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "客户订单号")
    @NotBlank(groups = {Create.class, Update.class}, message = "客户订单号不能为空")
    private String orderId;

    @ApiModelProperty(notes = "SF下单结果")
    @NotBlank(groups = {Create.class, Update.class}, message = "SF下单结果不能为空")
    private String orderResult;

    @ApiModelProperty(notes = "顺丰运单号")
    @NotBlank(groups = {Create.class, Update.class}, message = "顺丰运单号不能为空")
    private String mailno;

    @ApiModelProperty(notes = "筛单结果：1：人工确认 2：可收派 3：不可以收派")

    private String filterResult;

    @ApiModelProperty(notes = "原寄地区域代码")

    private String origincode;

    @ApiModelProperty(notes = "目的地区域代码")
    private String destcode;

    @ApiModelProperty(notes = "错误代码")

    private String errorCode;
    @ApiModelProperty(notes = "错误信息")

    private String errorMessage;
    @ApiModelProperty(notes = "如果filter_result=3时为必填，不可以收派的原因代码：1：收方超范围2：派方超范围3-：其它原因")

    private String remark;
    @ApiModelProperty(notes = "日期")

    private String syncDate;



    public interface Create {

    }

    public interface Update {

    }

}
