package org.crown.service;

import org.crown.common.result.Result;
import org.crown.framework.service.BaseService;
import org.crown.model.entity.CodeExpress;
import org.crown.model.entity.OrderSf;
import org.omg.CORBA.portable.UnknownException;


import java.util.List;
import java.util.Map;

public interface IExpressService extends BaseService<CodeExpress> {

    /**
     * 订单运单打印接口
     * @param paramMap
     * @return
     * @throws org.omg.CORBA.portable.UnknownException
     */

    //void printOrderService(Map<String, String> paramMap) throws UnknownException;

    //void printOrder1(Map<String, String> paramMap) throws UnknownException;

    /**
     * 签名
     * @param paramMap
     * @return
     * @throws UnknownException
     */
    Result<Map<String, String>> signParams(Map<String, String> paramMap) throws UnknownException;

    /**
     * 下订单接口
     * @param paramMap
     * @return
     * @throws UnknownException
     */
    Result<Map<String, String>> orderService(Map<String, String> paramMap) throws UnknownException;

    /**
     * 订单路由查询接口
     * @param paramMap
     * @return
     * @throws UnknownException
     */
    Result<Map<String,Map<String,String>>> routeOrderService(Map<String, String> paramMap) throws UnknownException;

    /**
     * 订单运单打印接口
     * @param paramMap
     * @return
     * @throws UnknownException
     */
    Result<Map<String, String>> printOrderService(Map<String, String> paramMap) throws UnknownException;

    // 查询快递单号
    Result<Map<String, String>> findSFOrderId(List<String> applyIds);

    List<OrderSf> findByApplyIds(List<String> applyIds);
}
