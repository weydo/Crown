package org.crown.service;

import org.crown.framework.service.BaseService;
import org.crown.model.entity.OrderLocal;

public interface IOrderLocalService extends BaseService<OrderLocal> {

    /**
     * 更新本地订单：快递单号  订单状态
     * @param orderLocal
     */
    void updateStateByOrderId(OrderLocal orderLocal);
}