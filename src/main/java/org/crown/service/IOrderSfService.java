package org.crown.service;

import org.crown.framework.service.BaseService;
import org.crown.model.entity.OrderSf;

public interface IOrderSfService extends BaseService<OrderSf> {

    /**
     * 获取菜单详情
     *
     * @param orderId
     * @return
     */
    OrderSf getByOrderId(String orderId);
}
