package org.crown.service.impl;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.crown.common.result.AbstractResult;
import org.crown.common.result.FailureResult;
import org.crown.common.result.Result;
import org.crown.common.result.SuccessResult;
import org.crown.common.utils.DateUtil;
import org.crown.common.utils.TestCallWaybillPrinter;
import org.crown.framework.service.impl.BaseServiceImpl;
import org.crown.mapper.CodeExpressMapper;
import org.crown.mapper.OrderSfMapper;
import org.crown.model.entity.CodeExpress;
import org.crown.model.entity.OrderLocal;
import org.crown.model.entity.OrderSf;
import org.crown.model.entity.PayGoods;
import org.crown.service.IExpressService;
import org.crown.service.IOrderLocalService;
import org.crown.service.IOrderSfService;
import org.jboss.logging.Logger;
import org.omg.CORBA.portable.UnknownException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.crown.common.utils.SFUtil;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.encoding.XMLType;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExpressServiceImpl extends BaseServiceImpl<CodeExpressMapper, CodeExpress> implements IExpressService {

    private static final Logger log = Logger.getLogger(ExpressServiceImpl.class);

    //@Value("${webServiceUrl}")
    //private String webServiceUrl;

    @Autowired
    private IOrderSfService orderSfService;

    @Autowired
    private IOrderLocalService orderLocalService;

    @Autowired
    private OrderSfMapper orderSfMapper;

/*    @Override
    @Transactional
    public void printOrderService(Map<String, String> paramMap){

        Map<String, String> map = new HashMap<String, String>();

        //PayGoods payGood = payGoodsMapper.selectByOrderId(paramMap.get("orderid"));
        PayGoods payGood = new PayGoods();

        //收件人信息
        payGood.setGoodsProvince(paramMap.get("goodsProvince"));//省
        payGood.setGoodsCity(paramMap.get("goodsCity"));//市
//        dto.setConsignerCounty("");//区
        payGood.setGoodsAddress(paramMap.get("goodsAddress")); //详细地址建议最多30个字  字段过长影响打印效果
        payGood.setName(paramMap.get("name"));//公司名称
        payGood.setGoodsPhone(paramMap.get("goodsPhone"));//收件人电话
        payGood.setGoodsName(paramMap.get("goodsName"));//收件人姓名
        //dto.setConsignerShipperCode("");
        payGood.setMode(paramMap.get("mode"));// 1-寄付 2-到付 3-第三方支付
        payGood.setGoodsZkeyNumber(paramMap.get("goodsZkeyNumber"));//主key数量
        payGood.setGoodsNote(paramMap.get("goodsNote"));//备注

        CodeExpress codeExpress = getById(paramMap.get("sfId"));//id序号：1->标识SF，获取寄件方信息

        OrderSf sf = new OrderSf();
        sf.setMailno(paramMap.get("mailNo"));//订单号
        sf.setDestcode(paramMap.get("destCode"));//目的地代码 参考顺丰地区编号
        sf.setOrigincode(paramMap.get("originCode"));//原寄地代码 参考顺丰地区编号


        try {
            map = TestCallWaybillPrinter.CAWayBillPrinterTools(payGood, codeExpress, sf);
        } catch (Exception e) {
            e.printStackTrace();
        }
*//*        PayGoods good = new PayGoods();
        //更新订单状态
        good.setGoodsState("3");
        good.setOrderNumber(paramMap.get("orderid"));
        payGoodsMapper.updateByOrderSelective(good);*//*

        return ;

    }*/

    @Override
    public Result<Map<String, String>> signParams(Map<String, String> paramMap) throws UnknownException {
        Map<String, String> map = new HashMap<String, String>();
        CodeExpress codeExpress = getById(1);//id序号：1->标识SF，获取寄件方信息
        //报文封装
        String xml = SFUtil.xmlHelper(paramMap,codeExpress);
        map.put("xml", xml);
        return new SuccessResult<Map<String,String>>(map);
    }

    @Override
    public Result<Map<String, String>> orderService(Map<String, String> paramMap) throws UnknownException {

        Map<String, String> map = new HashMap<String, String>();

        Result<Map<String, String>> _result = signParams(paramMap);
        System.out.println("_result"+_result.getData().get("xml"));

        org.apache.axis.client.Service service = new org.apache.axis.client.Service();

        Call call;
        OrderSf sf = new OrderSf();
        OrderLocal orderLocal = new OrderLocal();
        //String webServiceUrl = "http://bsp-oisp.sf-express.com/bsp-oisp/ws/expressService?wsdl";
        String webServiceUrl = "http://bsp-oisp.sf-express.com/bsp-oisp/ws/expressService";
        try {
            call=(Call) service.createCall();
            //设置命名空间和需要调用的方法名
            QName opAddEntry = new QName("http://service.expressservice.integration.sf.com/", "sfexpressService");
            //设置请求路径
            //call.setTargetEndpointAddress(ConfigHelper.getValue("webServiceUrl"));
            call.setTargetEndpointAddress(webServiceUrl);
            call.setOperationName(opAddEntry); //调用的方法名
            call.setTimeout(Integer.valueOf(2000));		//设置请求超时

            call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);//设置返回类型

            call.addParameter("arg0",XMLType.XSD_STRING,ParameterMode.IN);


            Object[] o = new Object[]{_result.getData().get("xml")};
            Object res= call.invoke(o);
            log.info("SF下单接口返回："+res.toString());
            //解析转码接口返回的xml报文
            map = SFUtil.xmlParse(res.toString());
            sf.setId(paramMap.get("orderid"));
            sf.setOrderId(paramMap.get("orderid"));
            sf.setOrderResult(map.get("order_result"));
            sf.setSyncDate(DateUtil.getDateTime());
            if("OK".equals(sf.getOrderResult())){
                sf.setMailno(map.get("mailno"));
                sf.setFilterResult(map.get("filter_result"));
                sf.setOrigincode(map.get("origincode"));
                sf.setDestcode(map.get("destcode"));
                sf.setRemark(map.get("remark"));
                //下单成功更新本地订单：快递单号，订单状态
                orderLocal.setOrderState("2");//1 未发货 2 生成订单 3 已发货 4 未领取 5 已领取
                orderLocal.setMailno(map.get("mailno"));
                orderLocal.setId(paramMap.get("orderid"));
                orderLocal.setOrderId(paramMap.get("orderid"));
                orderLocalService.updateStateByOrderId(orderLocal);
            }else{
                sf.setErrorCode(map.get("error_code"));
                sf.setErrorMessage(map.get("order_message"));
            }
            OrderSf orderSfNull = orderSfService.getByOrderId(paramMap.get("orderid"));
            if (orderSfNull == null) {
                if ("OK".equals(sf.getOrderResult())) {
                    //保存顺丰下订单相应结果
                    orderSfService.save(sf);
                    log.info("print begin!");
                    OrderSf orderSf = orderSfService.getByOrderId(paramMap.get("orderid"));//根据申请id查顺丰返回信息（取快递单号用于打印快递单）
                    if ("OK".equals(orderSf.getOrderResult())) {
                        CodeExpress codeExpress = getById(1);//id序号：1->标识SF，获取寄件方信息

                        OrderLocal orderLocal1 = orderLocalService.getById(paramMap.get("orderid"));//根据申请id查询本地订单信息
                        //更新本地系统订单发货状态
                        //orderLocal.setOrderId(sf.getOrderId());
                        //orderLocalService.updateStateByOrderId(orderLocal);
                        try {
                            map = TestCallWaybillPrinter.CWCAWayBillPrinterTools(codeExpress, orderSf, orderLocal1);
                            log.info("print end!");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                        //下单失败，再提示用户重新打印？ 2019-08-22
                        map.put("errMsg","顺丰下单失败！请重新打印！若仍失败，请联系研发攻城狮~~");
                        map.put("retCode","0");
                }
            }else{
/*                map.put("errMsg","已经打印过了！");
                map.put("retCode","0");*/
                //add by weydo 2019-08-22 修改误操作点击打印后，无法再打印的场景。
                log.info("重复打印：print begin!");
                OrderSf orderSf = orderSfService.getByOrderId(paramMap.get("orderid"));//根据申请id查顺丰返回信息（取快递单号用于打印快递单）
                if ("OK".equals(orderSf.getOrderResult())) {
                    CodeExpress codeExpress = getById(1);//id序号：1->标识SF，获取寄件方信息

                    OrderLocal orderLocal1 = orderLocalService.getById(paramMap.get("orderid"));//根据申请id查询本地订单信息
                    //更新本地系统订单发货状态
                    //orderLocal.setOrderId(sf.getOrderId());
                    //orderLocalService.updateStateByOrderId(orderLocal);
                    try {
                        map = TestCallWaybillPrinter.CWCAWayBillPrinterTools(codeExpress, orderSf, orderLocal1);
                        log.info("重复打印：print end!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getMessage());
        }
        return new SuccessResult<Map<String,String>>(map);
    }

    @Override
    public Result<Map<String,Map<String,String>>> routeOrderService(Map<String, String> paramMap) throws UnknownException {

        Map<String,Map<String,String>> map = new HashMap<String,Map<String,String>>();

        CodeExpress codeExpress = getById(1);//id序号：1->标识SF，获取寄件方信息
        //CodeExpress codeExpress = codeExpressMapper.selectByPrimaryKey(1);
        OrderSf sf = orderSfService.getByOrderId(paramMap.get("orderid"));//查
        //OrderSf sf = orderSfService.getById(paramMap.get("orderid"));
        //OrderSf sf = orderSfMapper.selectByOrderId(paramMap.get("orderid"));

        org.apache.axis.client.Service service = new org.apache.axis.client.Service();

        Call call;

        try {
            call=(Call) service.createCall();
            //设置命名空间和需要调用的方法名
            QName opAddEntry = new QName("http://service.expressservice.integration.sf.com/", "sfexpressService");
            //设置请求路径
            //call.setTargetEndpointAddress(ConfigHelper.getValue("webServiceUrl"));
            String webServiceUrl = "http://bsp-oisp.sf-express.com/bsp-oisp/ws/expressService";
            call.setTargetEndpointAddress(webServiceUrl);
            call.setOperationName(opAddEntry); //调用的方法名
            call.setTimeout(Integer.valueOf(2000));		//设置请求超时
            call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);//设置返回类型
            call.addParameter("arg0",XMLType.XSD_STRING,ParameterMode.IN);

            Object[] o = new Object[]{SFUtil.xmlRouteHelper(sf,codeExpress)};
            Object res= call.invoke(o);
            log.info("SF路由查询接口返回："+res.toString());
            String resp = SFUtil.xmlRouteResult(res.toString());
            map = SFUtil.xmlRouteParse(res.toString());
            if(!"OK".equals(resp)){
                Map<String,String> mmap = map.get("ERROR");
                return new FailureResult<Map<String,Map<String,String>>>(Integer.valueOf(mmap.get("error_code")), mmap.get("order_message"),map);
            }
        } catch (Exception e) {
            e.getStackTrace();
            log.error(e.getMessage());
        }
        return new SuccessResult<Map<String,Map<String,String>>>(map);
    }

    @Override
    @Transactional
    public Result<Map<String, String>> printOrderService(Map<String, String> paramMap) throws UnknownException{

        Map<String, String> map = new HashMap<String, String>();

        //PayGoods payGood = payGoodsMapper.selectByOrderId(paramMap.get("orderid"));
        PayGoods payGood = new PayGoods();

        //收件人信息
        payGood.setGoodsProvince(paramMap.get("goodsProvince"));//省
        payGood.setGoodsCity(paramMap.get("goodsCity"));//市
//        dto.setConsignerCounty("");//区
        payGood.setGoodsAddress(paramMap.get("goodsAddress")); //详细地址建议最多30个字  字段过长影响打印效果
        payGood.setName(paramMap.get("name"));//公司名称
        payGood.setGoodsPhone(paramMap.get("goodsPhone"));//收件人电话
        payGood.setGoodsName(paramMap.get("goodsName"));//收件人姓名
        //dto.setConsignerShipperCode("");
        payGood.setMode(paramMap.get("mode"));// 1-寄付 2-到付 3-第三方支付
        payGood.setGoodsZkeyNumber(paramMap.get("goodsZkeyNumber"));//主key数量
        payGood.setGoodsNote(paramMap.get("goodsNote"));//备注

        CodeExpress codeExpress = getById(paramMap.get("sfId"));//id序号：1->标识SF，获取寄件方信息

        OrderSf sf = new OrderSf();
        sf.setMailno(paramMap.get("mailNo"));//订单号
        sf.setDestcode(paramMap.get("destCode"));//目的地代码 参考顺丰地区编号
        sf.setOrigincode(paramMap.get("originCode"));//原寄地代码 参考顺丰地区编号


        try {
            map = TestCallWaybillPrinter.CAWayBillPrinterTools(payGood, codeExpress, sf);
        } catch (Exception e) {
            e.printStackTrace();
        }
/*        PayGoods good = new PayGoods();
        //更新订单状态
        good.setGoodsState("3");
        good.setOrderNumber(paramMap.get("orderid"));
        payGoodsMapper.updateByOrderSelective(good);*/

        return new SuccessResult<Map<String,String>>(map);

    }

    @Override
    public Result findSFOrderId(List<String> applyIds) {
        if (applyIds == null){
            return new FailureResult(-1,"参数为空");
        }
        Map<String,String> map = new HashMap<>();
        List<OrderSf> orders = findByApplyIds(applyIds);
        if (CollectionUtils.isEmpty(orders)){
            for (String applyId : applyIds){
                map.put(applyId,"未发货");
            }
            log.info("map={}");
            return new AbstractResult(0,"暂无快递信息",map);
        }
        Map<String, String> oMap = ordersToMap(orders);
        for (String applyId : applyIds){
            String status = oMap.getOrDefault(applyId, null);
            if (null != status){
                map.put(applyId,status);
            }else {
                map.put(applyId,"未发货");
            }
        }
        return new SuccessResult<Map<String,String>>("操作成功",map);
    }

    private Map<String,String> ordersToMap(List<OrderSf> orders) {
        Map<String,String> oMap = new HashMap<>();
        for (OrderSf order : orders){
            oMap.put(order.getId(),order.getMailno());
        }
        return oMap;
    }

    @Override
    public List<OrderSf> findByApplyIds(List<String> applyIds) {
        List<OrderSf> orders = orderSfMapper.selectList(new QueryWrapper<OrderSf>().in("id", applyIds));
        return orders;
    }
}
