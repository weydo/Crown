package org.crown.service.impl;

import org.crown.framework.service.impl.BaseServiceImpl;
import org.crown.mapper.OrderLocalMapper;
import org.crown.model.entity.OrderLocal;
import org.crown.service.IOrderLocalService;
import org.springframework.stereotype.Service;

@Service
public class OrderLocalServiceImpl  extends BaseServiceImpl<OrderLocalMapper, OrderLocal> implements IOrderLocalService {

    @Override
    public void updateStateByOrderId(OrderLocal orderLocal) {
        updateStateByOrderId(orderLocal);
    }

}
