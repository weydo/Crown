package org.crown.service.impl;


import java.util.List;

import org.crown.common.utils.TypeUtils;
import org.crown.framework.enums.ErrorCodeEnum;
import org.crown.framework.service.impl.BaseServiceImpl;
import org.crown.framework.utils.ApiAssert;
import org.crown.mapper.OrderSfMapper;
import org.crown.model.dto.OrderSfDTO;
import org.crown.model.entity.OrderSf;
import org.crown.service.IOrderSfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderSfServiceImpl  extends BaseServiceImpl<OrderSfMapper, OrderSf> implements IOrderSfService {


    @Autowired
    private IOrderSfService orderSfService;

    @Override
    public OrderSf getByOrderId(String orderId) {
        OrderSf orderSf = getById(orderId);
        return orderSf;
    }

}
