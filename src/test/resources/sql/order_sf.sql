/*
 Navicat MySQL Data Transfer

 Source Server         : localDB
 Source Server Type    : MySQL
 Source Server Version : 50557
 Source Host           : localhost:3306
 Source Schema         : crown1

 Target Server Type    : MySQL
 Target Server Version : 50557
 File Encoding         : 65001

 Date: 21/06/2019 15:21:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order_sf
-- ----------------------------
DROP TABLE IF EXISTS `order_sf`;
CREATE TABLE `order_sf`  (
  `id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单流水号',
  `order_result` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'SF下单结果',
  `mailno` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '顺丰运单号',
  `filter_result` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '筛单结果：1：人工确认 2：可收派 3：不可以收派',
  `origincode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原寄地区域代码',
  `destcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目的地区域代码',
  `error_code` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误代码',
  `error_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '错误信息',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '如果filter_result=3时为必填，不可以收派的原因代码：1：收方超范围2：派方超范围3-：其它原因',
  `sync_date` datetime NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Triggers structure for table order_sf
-- ----------------------------
DROP TRIGGER IF EXISTS `updateId`;
delimiter ;;
CREATE TRIGGER `updateId` AFTER INSERT ON `order_sf` FOR EACH ROW BEGIN
UPDATE order_sf SET id = order_id;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
